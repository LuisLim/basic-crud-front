$(document).ready( function () {
    var settings = {
        "url": "http://127.0.0.1:3333/person/" + localStorage.getItem('id'),
        "method": "GET",
        "timeout": 0,
        "processData": false,
        "contentType": "application/json"
    };

    $.ajax(settings).done(function (response) {
    console.log(response);
    let sub = response.person.person_birthdate.substring(0, 10)
    $("#person_name").val(response.person.person_name)
    $("#person_lastname").val(response.person.person_lastname)
    $("#person_birthdate").val(sub)
    $("#person_email").val(response.person.person_email)
    });
})

function updatePerson () {
    try {
        person = {
            person_name: $("#person_name").val(),
            person_lastname: $("#person_lastname").val(),
            person_birthdate: $("#person_birthdate").val(),
            person_email: $("#person_email").val(),
            person_password: $("#person_password").val()
        }

        var settings = {
        "url": "http://127.0.0.1:3333/person/" + localStorage.getItem('id'),
        "method": "PUT",
        "timeout": 0,
        "processData": false,
        "contentType": "application/json",
        "data": JSON.stringify({"person": person})
        };

        $.ajax(settings).done(function (response) {
        console.log(response);
        location.href = "index.html";
        });

    } catch (error) {
        console.log('Error', error)
    }
}

// var form = new FormData();
// form.append("empleado_sucursal_id", "1");
// form.append("empleado_puesto_id", "1");
// form.append("empleado_fecha_ingreso", "2021");
// form.append("empleado_sueldo_variable", "1");

// var settings = {
//   "url": "https://linx.oomovilapps.com/api/storeEmpleadoStepOne",
//   "method": "POST",
//   "timeout": 0,
//   "processData": false,
//   "mimeType": "multipart/form-data",
//   "contentType": false,
//   "data": form
// };

// $.ajax(settings).done(function (response) {
//   console.log(response);
// });