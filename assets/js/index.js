$(document).ready( function () {
    var settings = {
        "url": "http://127.0.0.1:3333/person",
        "method": "GET",
        "timeout": 0,
        "processData": false,
        "contentType": "application/json"
    };

    $.ajax(settings).done(function (response) {
    console.log(response);
    response.persons.forEach(element => {
            $("#tablePerson tbody").append(
                '<tr>' +
                    '<th scope="row">' + element.person_id + '</th>' +
                    '<td>' + element.person_name + '</td>' +
                    '<td>' + element.person_lastname + '</td>' +
                    '<td>' + dateFormat(element.person_birthdate) + '</td>' +
                    '<td>' + element.person_email + '</td>' +
                    '<td><button onclick="editPerson(' + element.person_id + ')">Editar</button> <button onclick="deletePerson(' + element.person_id + ')">Borrar</button></td>' +
                '</tr>'
            )
        });
    });
})

function dateFormat (x) {
    x.toString()
    let result = x.substring(0, 10)
    return result
}

function editPerson (id) {
    localStorage.setItem('id', id)
    location.href = "update.html"
}

function deletePerson (id) {
    localStorage.setItem('id', id)
    $("#deleteModal").modal('toggle')
    console.log(id)
}

function toggleModal () {
    $("#deleteModal").modal('toggle')
}

function deleteConfirm () {
    var settings = {
        "url": "http://127.0.0.1:3333/person/" + localStorage.getItem('id'),
        "method": "DELETE",
        "timeout": 0,
        "processData": false,
        "contentType": "application/json"
    };

    $.ajax(settings).done(function (response) {
    location.reload()
    });
}

// var form = new FormData();
// form.append("empleado_sucursal_id", "1");
// form.append("empleado_puesto_id", "1");
// form.append("empleado_fecha_ingreso", "2021");
// form.append("empleado_sueldo_variable", "1");

// var settings = {
//   "url": "https://linx.oomovilapps.com/api/storeEmpleadoStepOne",
//   "method": "POST",
//   "timeout": 0,
//   "processData": false,
//   "mimeType": "multipart/form-data",
//   "contentType": false,
//   "data": form
// };

// $.ajax(settings).done(function (response) {
//   console.log(response);
// });