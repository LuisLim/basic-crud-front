function addPerson () {
    try {
        person = {
            person_name: $("#person_name").val(),
            person_lastname: $("#person_lastname").val(),
            person_birthdate: $("#person_birthdate").val(),
            person_email: $("#person_email").val(),
            person_password: $("#person_password").val()
        }

        var settings = {
        "url": "http://127.0.0.1:3333/person",
        "method": "POST",
        "timeout": 0,
        "processData": false,
        "contentType": "application/json",
        "data": JSON.stringify({"person": person})
        };

        $.ajax(settings).done(function (response) {
        console.log(response);
        location.href = "index.html";
        });

    } catch (error) {
        console.log('Error', error)
    }
}